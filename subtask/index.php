<?php
// checking for minimum PHP version
if (version_compare(PHP_VERSION, '5.3.7', '<')) {
    exit("Sorry, Simple PHP Login does not run on a PHP version smaller than 5.3.7 !");
} else if (version_compare(PHP_VERSION, '5.5.0', '<')) {
    // if you are using PHP 5.3 or PHP 5.4 you have to include the password_api_compatibility_library.php
    // (this library adds the PHP 5.5 password hashing functions to older versions of PHP)
    require_once("includes/libraries/password_compatibility_library.php");
}

// include the configs / constants for the database connection
require_once("../includes/dbconx.php");

// load the login class
require_once("../includes/classes/Login.php");

// create a login object. when this object is created, it will do all login/logout stuff automatically
// so this single line handles the entire login process. in consequence, you can simply ...
$login = new Login();

if ($login->isUserLoggedIn() == false) {
	// If the user is not logged in, take them to the login Page
	header("Location: ../login/");
	$this->errors[] = "You need to be logged in to do that.";
	die();
	
}

?>

<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en" ng-app="todomvc" data-framework="firebase"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Subtask - Your Simple, Free To-Do Lists</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <link rel="stylesheet" href="../css/bootstrap.css">
		<style>[ng-cloak] { display: none; }</style>
        <link rel="stylesheet" href="../css/subtask.css">	
    </head>
    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
    
    
    
    <nav class="navbar" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-click" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="../">
	        	<svg class="svg-logo" width="54" height="40">
					<image xlink:href="../img/nav-logo.svg" src="img/nav-logo.png" width="54" height="40" />
				</svg>
	        	<p class="logo-text">Subtask<p>
		  </a>
        </div>
        <div class="navbar-collapse collapse navbar-right navbar-click">
        	<a class="navlink " href="../">Home</a>
			<a class="navlink" href="../about/">About</a>
			<a class="navlink navlink-active" href="#">Subtask</a>
			<?php   if ($login->isUserLoggedIn() == true) {  ?>
					<a class="navlink" href="../index.php?logout">Log Out</a>
			<?php } else { ?>
					<a class="navlink" href="../login/">Log In</a>
			<?php } ?>
        </div><!--/.navbar-collapse -->
      </div>
    </nav>
    
    


		
		
		
		
		
		
		
		
		
	<div class="container" ng-controller="TodoCtrl">
		<div class="row row-offcanvas row-offcanvas-left">

        	<div class="col-xs-6 col-sm-3 sidebar-offcanvas" id="sidebar" role="navigation">
        		<div class="custom-sidebar">
	        		<div class="user-section">
		        		<a href="#" class="username"><?php echo $_SESSION['user_name']; ?></a>
	        		</div>
	        		<div class="projects-section">
		        		<form id="searchbar">
							<input type="search" placeholder="Search"  ng-model="query">
						</form>
						
						<div class="project-container">
							<div class="projects">
								<div class="projects-title">
									<h4>Project Title</h4>
									<button class='btn pull-right delete-project' ng-click="removeTodo(todo)"></button>
								</div>
								<ul>
									<li class="lists-title">List Title
										<button class='btn pull-right delete-list' ng-click="removeTodo(todo)"></button>
									</li>
									<li class="lists-title">List Title
										<button class='btn pull-right delete-list' ng-click="removeTodo(todo)"></button>
									</li>
									<li class="lists-title">List Title
										<button class='btn pull-right delete-list' ng-click="removeTodo(todo)"></button>
									</li>
								</ul>
							</div>
							<div class="projects">
								<div class="projects-title">
									<h4>Project Title</h4>
									<button class='btn pull-right delete-project' ng-click="removeTodo(todo)"></button>
								</div>
								<ul>
									<li class="lists-title">List Title
										<button class='btn pull-right delete-list' ng-click="removeTodo(todo)"></button>
									</li>
									<li class="lists-title">List Title
										<button class='btn pull-right delete-list' ng-click="removeTodo(todo)"></button>
									</li>
									<li class="lists-title">List Title
										<button class='btn pull-right delete-list' ng-click="removeTodo(todo)"></button>
									</li>
								</ul>
							</div>
							<div class="projects">
								<div class="projects-title">
									<h4>List Title</h4>
									<button class='btn pull-right delete-project' ng-click="removeTodo(todo)"></button>
								</div>
							</div>
							
						</div>
						<div class="project-button-container">
							<a href="#new" class="project-button">+</a>
						</div>
	        		</div>
	        	</div>
        	</div>
        
        
			<div class="col-xs-12 col-sm-9 content">
				<p class="pull-left">
					<button type="button" class="sidebar-button" data-toggle="offcanvas">Toggle nav</button>
				</p>
  
			<section >
				<section ng-show="totalCount" ng-cloak>
					<h3>Things To Do</h3>
					<div class="shared-list">
						<span class="list-owner"><?php echo $_SESSION['user_name']; ?></span>
						Shared with 
						<span class="list-owner">User 1, User 2</span>
					</div>
					<div class="list-group">
						<div ng-repeat="todo in todos | filter:query " class="list-group-item" ng-class="{editing: todo == editedTodo}">
							<div for="{{todo.title}}" class="view" ng-if='todo !== editedTodo'>
								<input id="{{todo.title}}" type="checkbox" ng-model="todo.completed" ng-change="todos.$save(todo)">

								
								<label ng-dblclick="editTodo(todo)" ng-if='todo.completed' ng-style="{'text-decoration': 'line-through'}">{{todo.title}}</label>
								<label ng-dblclick="editTodo(todo)" ng-if='!todo.completed' ng-style="{'text-decoration': 'none'}">{{todo.title}}</label>

								<button class='btn pull-right delete-task' ng-click="removeTodo(todo)"></button>
							</div>
							<form ng-submit="doneEditing(todo)" ng-if='todo == editedTodo'>
								<input class="edit" ng-model="todo.title" todo-blur="doneEditing(todo)" todo-focus="todo == editedTodo">
							</form>
						</div>
					</div>
					<div class='btn btn-default pull-right' ng-click='clearCompletedTodos()'>Clear completed</div>
					<div class="mark-all">
						<!-- Mark all todos as complete -->
						<input id="toggle-all" type="checkbox" ng-model="allChecked" ng-click="markAll(allChecked)">
						<label for="toggle-all">Mark all as complete</label>
					</div>
				</section>
				<header class="add-task">
					<span><strong>{{remainingCount}}</strong> things todo</span>
					<form ng-submit="addTodo()">
						<input class='form-control' placeholder="What needs to be done?" ng-model="newTodo" autofocus>
					</form>
				</header>
			</section>
			</div>
		</div>

		<!-- AngularJS -->
		<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.2/angular.min.js"></script>
		<!-- Firebase -->
		<script src="https://cdn.firebase.com/js/client/2.0.4/firebase.js"></script>
		<!-- AngularFire -->
		<script src="https://cdn.firebase.com/libs/angularfire/0.9.0/angularfire.min.js"></script>
		<script src="app.js"></script>		
		
    </div> <!-- /container -->        
    
    	
    	<!-- jQuery Code -->
    	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.2.min.js"><\/script>')</script>
		<!-- Bootstrap Code -->
        <script src="../js/vendor/bootstrap.min.js"></script>	
		<!-- Custom Javascript Code -->
		<script src="../js/main.js"></script>
        <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
        <script>
            (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
            function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
            e=o.createElement(i);r=o.getElementsByTagName(i)[0];
            e.src='//www.google-analytics.com/analytics.js';
            r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
            ga('create','UA-XXXXX-X','auto');ga('send','pageview');
        </script>
    </body>
</html>
