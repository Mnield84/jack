<?php
// checking for minimum PHP version
if (version_compare(PHP_VERSION, '5.3.7', '<')) {
    exit("Sorry, Simple PHP Login does not run on a PHP version smaller than 5.3.7 !");
} else if (version_compare(PHP_VERSION, '5.5.0', '<')) {
    // if you are using PHP 5.3 or PHP 5.4 you have to include the password_api_compatibility_library.php
    // (this library adds the PHP 5.5 password hashing functions to older versions of PHP)
    require_once("includes/libraries/password_compatibility_library.php");
}

// include the configs / constants for the database connection
require_once("../includes/dbconx.php");

// load the login class
require_once("../includes/classes/Login.php");

// create a login object. when this object is created, it will do all login/logout stuff automatically
// so this single line handles the entire login process. in consequence, you can simply ...
$login = new Login();

// ... ask if we are logged in here:
if ($login->isUserLoggedIn() == true) {
	// If the user is logged in, take them to the Subtask Page
	header("Location: ../subtask/");
	die();
}
?>

<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Subtask - Log In to Your Account</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="apple-touch-icon" href="../apple-touch-icon.png">

        <link rel="stylesheet" href="../css/bootstrap.css">
        <link rel="stylesheet" href="../css/login.css">

        <script src="../js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
    </head>
    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
    <nav class="navbar" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="../">
	        	<svg class="svg-logo" width="54" height="40">
					<image xlink:href="../img/nav-logo.svg" src="../img/nav-logo.png" width="54" height="40" />
				</svg>
	        	<p class="logo-text">Subtask<p>
		  </a>
        </div>
        <div id="navbar" class="navbar-collapse collapse navbar-right">
          <a class="navlink" href="../">Home</a>
          <a class="navlink" href="#">About</a>
          <a class="navlink" href="../subtask/">Subtask</a>
          <?php   if ($login->isUserLoggedIn() == true) { 
					// the user is logged in. you can do whatever you want here.
					// for demonstration purposes, we simply show the "you are logged in" view. ?>
					<a class="navlink" href="?logout">Log Out</a>
			<?php } else {
					// the user is not logged in. you can do whatever you want here.
					// for demonstration purposes, we simply show the "you are not logged in" view.  ?>
					<a class="navlink navlink-active" href="../login">Log In</a>
			<?php } ?>
        </div><!--/.navbar-collapse -->
      </div>
    </nav>
    
    
    <div class="container content">
    	<!-- Example row of columns -->
		<div class="row">
			<div class="col-xs-offset-3 col-xs-6 col-sm-offset-3 col-sm-6 col-md-offset-4 col-md-4 col-lg-offset-4 col-lg-4">
				<?php 
					// ... ask if we are logged in here:
					if ($login->isUserLoggedIn() == true) {
						// the user is logged in. you can do whatever you want here.
						// for demonstration purposes, we simply show the "you are logged in" view.
						include("../includes/views/logged_in.php");
						
						} else {
						// the user is not logged in. you can do whatever you want here.
						// for demonstration purposes, we simply show the "you are not logged in" view.
						include("../includes/views/not_logged_in.php");
					}
			?>

			</div>
		</div>
    </div> <!-- /container -->        
    <footer>
	    <div class="container">
	    	<div class="row">
				<div class="col-sm-6 ">
					<p class="text-footer navbar-left">&copy; Subtask 2015 <span class="dont-steal">- Please dont steal</span></p>
        		</div>
				<div class="col-sm-6 ">
					<div class="sitemap-footer navbar-right">
						<a href="../">Home</a>  |  
						<a href="../about/">About</a>  |  
						<a href="../subtask/">Subtask</a>  |  
						<a href="#">Share</a>
        			</div>
        		</div>
			</div>
	    </div>
      </footer>	
    	
    	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.2.min.js"><\/script>')</script>

        <script src="../js/vendor/bootstrap.min.js"></script>

        <script src="../js/main.js"></script>

        <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
        <script>
            (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
            function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
            e=o.createElement(i);r=o.getElementsByTagName(i)[0];
            e.src='//www.google-analytics.com/analytics.js';
            r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
            ga('create','UA-XXXXX-X','auto');ga('send','pageview');
        </script>
    </body>
</html>
