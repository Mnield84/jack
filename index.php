<?php
// checking for minimum PHP version
if (version_compare(PHP_VERSION, '5.3.7', '<')) {
    exit("Sorry, Simple PHP Login does not run on a PHP version smaller than 5.3.7 !");
} else if (version_compare(PHP_VERSION, '5.5.0', '<')) {
    // if you are using PHP 5.3 or PHP 5.4 you have to include the password_api_compatibility_library.php
    // (this library adds the PHP 5.5 password hashing functions to older versions of PHP)
    require_once("includes/libraries/password_compatibility_library.php");
}

// include the configs / constants for the database connection
require_once("includes/dbconx.php");

// load the login class
require_once("includes/classes/Login.php");

// create a login object. when this object is created, it will do all login/logout stuff automatically
// so this single line handles the entire login process. in consequence, you can simply ...
$login = new Login();

?>

<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en" ng-app="Subtask"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Subtask - Your Simple, Free To-Do Lists</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <link rel="stylesheet" href="css/bootstrap.css">
        <link rel="stylesheet" href="css/main.css">
        <script src="js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
        <!-- AngularJS -->
		<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>
		<!-- Firebase -->
		<script src="https://cdn.firebase.com/js/client/2.2.2/firebase.js"></script>
		<!-- AngularFire -->
		<script src="https://cdn.firebase.com/libs/angularfire/1.0.0/angularfire.min.js"></script>
    </head>
    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
    <nav class="navbar-2" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-click" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar header-icon"></span>
            <span class="icon-bar header-icon"></span>
            <span class="icon-bar header-icon"></span>
          </button>
          <a class="navbar-brand-header navbar-brand" href=".">
	        	<svg class="svg-logo" width="54" height="40">
					<image xlink:href="img/logo-header.svg" src="img/header-nav-logo.png" width="54" height="40" />
				</svg>
	        	<p class="logo-header">Subtask<p>
		  </a>
        </div>
        <div class="navbar-collapse collapse navbar-right navbar-click">
        	<a class="navlink navlink-active" href=".">Home</a>
			<a class="navlink" href="about/">About</a>
			<a class="navlink" href="subtask/">Subtask</a>
			<?php   if ($login->isUserLoggedIn() == true) { 
					// the user is logged in. you can do whatever you want here.
					// for demonstration purposes, we simply show the "you are logged in" view. ?>
					<a class="navlink" href="?logout">Log Out</a>
			<?php } else {
					// the user is not logged in. you can do whatever you want here.
					// for demonstration purposes, we simply show the "you are not logged in" view.  ?>
					<a class="navlink" href="login/">Log In</a>
			<?php } ?>

          
        </div><!--/.navbar-collapse -->
      </div>
    </nav>

    <div class="full-width ">
	    <div class="container">
			<div class="row">
		        <div class="col-xs-offset-3 col-xs-6 col-sm-offset-3 col-sm-6 col-md-offset-4 col-md-4 col-lg-offset-4 col-lg-4 header-content">
		        	<svg class="header-logo">
						<image class="big-logo" xlink:href="img/header-logo2.svg" src="img/header-logo.png" width="170" height="120" />
					</svg>
		        	<h1>Subtask</h1>
		        	<?php   if ($login->isUserLoggedIn() == true) { 
						// the user is logged in. you can do whatever you want here.
						// for demonstration purposes, we simply show the "you are logged in" view. ?>
						<a class="btn-subtask center-block" href="subtask/"><?php echo $_SESSION['user_name']; ?>, Start Completing Tasks</a>
						<p class="text-header text-center">Not  <span class="text-user"><?php echo $_SESSION['user_name']; ?></span>?</p>
						<a href="?logout" class="link-header link-about center-block">Sign Out</a>
					<?php } else {
						// the user is not logged in. you can do whatever you want here.
						// for demonstration purposes, we simply show the "you are not logged in" view.  ?>
						<div class="btn-header center-block">
							<div class="hide-click">
								<h2 class="login-header">Log In</h2>
								<form class="form-login" action="login/index.php" method="post" accept-charset="utf-8">
									<label for="login_input_username">Username</label>
									<input id="login_input_username" class="login_input" type="text" name="user_name" placeholder="Username" required />
									<label for="login_input_password">Password</label>
									<input id="login_input_password" class="login_input" type="password" name="user_password" autocomplete="off" placeholder="Password" required />
									<input type="submit"  name="login" value="Log In" />
								</form>	
								<p class="text-click">Don&#39;t have an account? <a href="login/register.php" class="form-sign-up">Sign Up</a></p>		
							</div>
		        		</div>
						<p class="text-header text-center">or</p>
						<a href="login/register.php" class="link-header center-block">Sign Up</a>
					<?php } ?>		        	
		        </div>
		    </div>
	    </div>
    </div>
    
    <nav class="navbar" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-click" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href=".">
	        	<svg class="svg-logo" width="54" height="40">
					<image xlink:href="img/nav-logo.svg" src="img/nav-logo.png" width="54" height="40" />
				</svg>
	        	<p class="logo-text">Subtask<p>
		  </a>
        </div>
        <div class="navbar-collapse collapse navbar-right navbar-click">
          <a class="navlink navlink-active" href=".">Home</a>
          <a class="navlink" href="about/">About</a>
          <a class="navlink" href="#">Subtask</a>
          <?php   if ($login->isUserLoggedIn() == true) { 
					// the user is logged in. you can do whatever you want here.
					// for demonstration purposes, we simply show the "you are logged in" view. ?>
					<a class="navlink" href="?logout">Log Out</a>
			<?php } else {
					// the user is not logged in. you can do whatever you want here.
					// for demonstration purposes, we simply show the "you are not logged in" view.  ?>
					<a class="navlink" href="login/">Log In</a>
			<?php } ?>
        </div><!--/.navbar-collapse -->
      </div>
    </nav>
    
    
    <div class="container content">
    	<!-- Example row of columns -->
		<div class="row">
			<div class="col-md-offset-4 col-md-4">
				<h2>Subtask</h2>
				<h3>Some Small Tagline goes here</h3>
        	</div>
		</div>
		<div class="row">
				<div class="main-image col-sm-12 ">
        		</div>
			</div>
		<div class="row">
	        <div class="info-box col-xs-6 col-sm-4 col-md-3">
		        <div class="icon center-block">
			        <svg class="svg-logo" width="32" height="32">
						<image xlink:href="img/icon/free.svg" src="img/icon/free.png" width="32" height="32" />
					</svg>
		        </div>
		        <h4>Free To Use For Everyone</h4>
				<p>No sign up or subscription fees, just log in and start making lists.</p>
	        </div>
	        <div class="info-box col-xs-6 col-sm-4 col-md-3">
		        <div class="icon center-block">
			        <svg class="svg-logo" width="32" height="32">
						<image xlink:href="img/icon/share.svg" src="img/icon/share.png" width="32" height="32" />
					</svg>
		        </div>
		        <h4>Collaborate On Lists</h4>
				<p>Share your lists with everyone.</p>
	        </div>
	        <div class="info-box col-xs-6 col-sm-4 col-md-3">
		        <div class="icon center-block">
			        <svg class="svg-logo" width="32" height="32">
						<image xlink:href="img/icon/comment.svg" src="img/icon/comment.png" width="32" height="32" />
					</svg>
		        </div>
		        <h4>Comments</h4>
				<p>Add a little bit extra information to your tasks.</p>
	        </div>
	        <div class="info-box col-xs-6 col-sm-4 col-md-3">
		        <div class="icon center-block">
			        <svg class="svg-logo" width="32" height="32">
						<image xlink:href="img/icon/devices.svg" src="img/icon/devices.png" width="32" height="32" />
					</svg>
		        </div>
		        <h4>On All Your Devices</h4>
				<p>The app responds for all of your devices, so you have your lists with you.</p>
	        </div>
	        <div class="info-box col-xs-6 col-sm-4 col-md-3">
		        <div class="icon center-block">
			        <svg class="svg-logo" width="32" height="32">
						<image xlink:href="img/icon/projects.svg" src="img/icon/projects.png" width="32" height="32" />
					</svg>
		        </div>
		        <h4>Projects</h4>
				<p>Keep your lists organised by project.</p>
	        </div>
	        <div class="info-box col-xs-6 col-sm-4 col-md-3">
		        <div class="icon center-block">
			        <svg class="svg-logo" width="32" height="32">
						<image xlink:href="img/icon/time.svg" src="img/icon/time.png" width="32" height="32" />
					</svg>
		        </div>
		        <h4>Due Dates</h4>
				<p>Make sure you get your tasks done before the deadline.</p>
	        </div>
	        <div class="info-box col-xs-6 col-sm-4 col-sm-offset-2 col-md-3 col-md-offset-0">
				<div class="icon center-block">
					<svg class="svg-logo" width="32" height="32">
						<image xlink:href="img/icon/list.svg" src="img/icon/list.png" width="32" height="32" />
					</svg>
				</div>
		        <h4>List Everything</h4>
				<p>If you can think of it, it can be listed.</p>
	        </div>
	        <div class="info-box col-xs-6 col-sm-4 col-md-3">
		        <div class="icon center-block">
			        <svg class="svg-logo" width="32" height="32">
						<image xlink:href="img/icon/layout.svg" src="img/icon/layout.png" width="32" height="32" />
					</svg>
		        </div>
		        <h4>Simple Design</h4>
				<p>Everything you need is in the sidebar.</p>
	        </div> 
		</div>  
		<div class="row">
			<div class="col-sm-4 col-sm-offset-4 col-xs-8 col-xs-offset-2">
				<p class="text-signup">Start making lists now</p>
				<?php   if ($login->isUserLoggedIn() == true) { 
					// the user is logged in. you can do whatever you want here.
					// for demonstration purposes, we simply show the "you are logged in" view. ?>
					<a href="subtask/" class="btn-signup center-block">Subtask</a>
				<?php } else {
					// the user is not logged in. you can do whatever you want here.
					// for demonstration purposes, we simply show the "you are not logged in" view.  ?>
					<a href="login/register.php" class="btn-signup center-block">Sign Up</a>
				<?php } ?>
        	</div>
		</div>


      
    </div> <!-- /container -->        
    <footer>
	    <div class="container">
	    	<div class="row">
				<div class="col-sm-6 ">
					<p class="text-footer navbar-left">&copy; Subtask 2015 <span class="dont-steal">- Please dont steal</span></p>
        		</div>
				<div class="col-sm-6 ">
					<div class="sitemap-footer navbar-right">
						<a href="#">Home</a>  |  
						<a href="#">About</a>  |  
						<a href="#">Subtask</a>  |  
						<a href="#">Share</a>
        			</div>
        		</div>
			</div>
	    </div>
      </footer>	
    	
    	<!-- jQuery Code -->
    	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.2.min.js"><\/script>')</script>

        <script src="js/vendor/bootstrap.min.js"></script>
        <script src="js/main.js"></script>
        

        <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
        <script>
            (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
            function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
            e=o.createElement(i);r=o.getElementsByTagName(i)[0];
            e.src='//www.google-analytics.com/analytics.js';
            r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
            ga('create','UA-XXXXX-X','auto');ga('send','pageview');
        </script>
    </body>
</html>
